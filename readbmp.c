#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "bmp.h"

int main()
{
struct IMAGE img;
unsigned char *data;
unsigned int bytesPerPixel;
ReadImage(&data,&img, &bytesPerPixel);
WriteImage(&data, img, bytesPerPixel);
free(data);
return 0;
}
