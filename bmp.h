#define DATA_OFFSET_OFFSET 0x000A
#define WIDTH_OFFSET 0x0012
#define HEIGHT_OFFSET 0x0016
#define BITS_PER_PIXEL_OFFSET 0x001C
#define HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define NO_COMPRESION 0
#define MAX_NUMBER_OF_COLORS 0
#define ALL_COLORS_REQUIRED 0

typedef struct IMAGE{
     unsigned int width;
     unsigned int height;
     unsigned int bitsPerPixel;
}IMAGE ;

void ReadImage(unsigned char **data , struct IMAGE *img, unsigned int *bytesPerPixel)
{
    FILE *imageFile = fopen(data, "rb");
    printf("READING FILE\n");
    unsigned int dataOffset;
    fseek(imageFile, DATA_OFFSET_OFFSET, SEEK_SET);
    fread(&dataOffset, 4, 1, imageFile);

    fseek(imageFile, WIDTH_OFFSET, SEEK_SET);
            fread(&img->width, 4, 1, imageFile);

    fseek(imageFile, HEIGHT_OFFSET, SEEK_SET);
            fread(&img->height, 4, 1, imageFile);

    fseek(imageFile, BITS_PER_PIXEL_OFFSET, SEEK_SET);
        fread(&img->bitsPerPixel, 2, 1, imageFile);

*bytesPerPixel = ((int)img->bitsPerPixel) / 8;

int paddedRowSize =(int)(4 * ceil((float)(img->width) / 4.0f))*(*bytesPerPixel);
int unpaddedRowSize = (img->width)*(*bytesPerPixel);
int totalSize = unpaddedRowSize*(img->height);
data = (unsigned char)malloc(totalSize);

int i = 0;
unsigned char *currentRowPointer = *data+((img->height-1)*unpaddedRowSize);
for (i = 0; i < img->height; i++)
        {
                fseek(imageFile, dataOffset+(i*paddedRowSize), SEEK_SET);
            fread(currentRowPointer, 1, unpaddedRowSize, imageFile);
            currentRowPointer -= unpaddedRowSize;
        }
 fclose(imageFile);
}

void WriteImage(unsigned char *data, IMAGE img, unsigned int bytesPerPixel)
{
    FILE *outputFile = fopen(data, "wb");
    printf("WRITTING FILE\n");
    const char *BM = "BM";
        fwrite(&BM[0], 1, 1, outputFile);
        fwrite(&BM[1], 1, 1, outputFile);

        int paddedRowSize = (int)(4 * ceil((float)(img.width)/4.0f))*bytesPerPixel;

        unsigned int fileSize = paddedRowSize*img.height + HEADER_SIZE + INFO_HEADER_SIZE;
        fwrite(&fileSize, 4, 1, outputFile);

        unsigned int reserved = 0x0000;
        fwrite(&reserved, 4, 1, outputFile);

        unsigned int dataOffset = HEADER_SIZE+INFO_HEADER_SIZE;
        fwrite(&dataOffset, 4, 1, outputFile);

        unsigned int infoHeaderSize = INFO_HEADER_SIZE;
        fwrite(&infoHeaderSize, 4, 1, outputFile);

        fwrite(&img.width, 4, 1, outputFile);
        fwrite(&img.height, 4, 1, outputFile);

        int planes = 1;
        fwrite(&planes, 2, 1, outputFile);

        int bitsPerPixel = bytesPerPixel * 8;
        fwrite(&bitsPerPixel, 2, 1, outputFile);

        unsigned int compression = NO_COMPRESION;
        fwrite(&compression, 4, 1, outputFile);

        unsigned int imageSize = (img.width)*(img.height)*(bytesPerPixel);
        fwrite(&imageSize, 4, 1, outputFile);

        unsigned int resolutionX = 11811;
        unsigned int resolutionY = 11811;

        fwrite(&resolutionX, 4, 1, outputFile);
        fwrite(&resolutionY, 4, 1, outputFile);

        unsigned int colorsUsed = MAX_NUMBER_OF_COLORS;
        fwrite(&colorsUsed, 4, 1, outputFile);

        unsigned int importantColors = ALL_COLORS_REQUIRED;
        fwrite(&importantColors, 4, 1, outputFile);


        int i = 0;
        int unpaddedRowSize =(img.width)*(bytesPerPixel);
        for ( i = 0; i < img.height; i++)
        {
                int pixelOffset = ((img.height - i) - 1)*unpaddedRowSize;
                fwrite(&data[pixelOffset], 1, paddedRowSize, outputFile);
        }
fclose(outputFile);
}

